package unit_tests

import (
	"testing"
	"example"
)

func TestHello(t *testing.T) {
	if example.Hello() != "World" {
		t.Fail()
	}
}
